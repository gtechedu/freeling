#!/bin/bash

docker stop $(docker ps -q --filter "expose=5432") $(docker ps -q --filter "expose=80") $(docker ps -q --filter "expose=3030")
docker rm $(docker ps -a -f status=exited -q)

docker run -it -p 80:80 -v $PWD/www:/var/www/html enyalius/freeling 
#docker run -it -p 80:80 enyalius/freeling 