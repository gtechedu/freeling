#!/bin/bash

analyze -f pt.cfg --server --port 50005 --output json  &
analyze -f es.cfg --server --port 50006 --output json  &
analyze -f en.cfg --server --port 50007 --output json  &

apachectl -D FOREGROUND