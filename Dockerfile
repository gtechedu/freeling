FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=America/Sao_Paulo
ENV FREELINGSHARE=/usr/share/freeling
RUN apt update && apt-get install -y curl apache2 php locales libboost-filesystem1.71.0 libboost-iostreams1.71.0 libboost-program-options1.71.0 libboost-regex1.71.0 libboost-system1.71.0 libboost-thread1.71.0 libicu66 tzdata
RUN locale-gen en_US.UTF-8
#&& apt-get install build-essential cmake 

WORKDIR /tmp
RUN     curl -L -o freeling.deb  https://github.com/TALP-UPC/FreeLing/releases/download/4.2/freeling-4.2-focal-amd64.deb && \
        dpkg -i freeling.deb && \
        rm -rf freeling.deb /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/www/html/z_data/* /var/www/html/index.html


COPY www/. /var/www/html/
COPY run.sh /bin/serverFreeling 
RUN  chmod +x /bin/serverFreeling

WORKDIR /var/www/html

EXPOSE 80
CMD serverFreeling