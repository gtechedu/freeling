<?php

require 'util.php';

$ct = new Control();
$ct->processText();

class Control
{
    private $dir = '/tmp/';
    //processos do Freeling
    private $langs = [
        'pt' => 50005,
        'es' => 50006,
        'pn' => 50007
    ];

    public function processText()
    {

        $file = '';
        if (!isset($_REQUEST['text'])) {
            return serviceError('É necessário informar o texto');
        }
        
        $file = $this->dir . uniqid();
        $lang = $this->langs['pt'];
        if(isset($_REQUEST['lang'])){
            $lang =  $this->getLang($_REQUEST['lang']);
        }
        file_put_contents($file, $_REQUEST['text']);
        

        $exec = 'analyzer_client localhost:' . $lang . ' <' . $file ;
        //echo $exec;
        exec($exec, $output, $returnvar);
        if ($returnvar == 0) {
            foreach ($output as $linha) {
                echo $linha;
            }
        } else {
            echo 'no return';
        }
        //unlink($file);
    }

    private function getLang($lang){
        $lang = trim(strtolower($lang));
        if(isset($this->langs[$lang])){
            return $this->langs[$lang];
        }
        return $this->langs['pt'];
    }
}

