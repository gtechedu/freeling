<?php
/**
 * Arquivo que possui as melhores funções e comandos espertos do Enyalius.
 *  
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
header('Access-Control-Allow-Origin: *');
ini_set('default_charset', 'utf-8');
//mb_internal_encoding('utf-8');  //Instalar o mb no servidor antes de liberar ver se será necessário.
//mb_detect_order('utf-8');
setlocale(LC_ALL, 'pt_BR.UTF-8', 'Portuguese_Brazil.1252');


function serviceError($mensagem, $erro = 400){
    http_response_code($erro);
    print '{error: "'. $mensagem .'"}';
}

function processaRequisicao(){
    $url = filter_var($_SERVER['REQUEST_URI']);
    list($caminho, $args) = explode('.php', $url);// Separa o caminho dos argumentos
    $args = explode('/', $args); //Quebra o argumentos em partes

    $control = new Control();
    if (isset($args[1]) && method_exists($control, $args[1])) {
        $control->{$args[1]}(array_slice($args, 2));
    } else {
        serviceError('Função desconhecida');
    }
}

function sanitizeString($texto){
    $data = strip_tags($texto);
    $data = filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS);
    $data = preg_replace('/[^0-9A-zÀ-ú]/', '', $data);
    return $data;
}

function preparaTexto($texto){

   // $texto = preg_replace('/[^0-9A-zÀ-ú\s.]/', ' ', $texto);

    $texto = html_entity_decode($texto);
    $texto = strip_tags($texto);

    return $texto;
}