<?php

require 'util.php';

class Control
{
    private $dir = '/tmp/';

    public function processText()
    {

        $file = '';
        $conf = isset($_REQUEST['conf']) ? $_REQUEST['conf'] : '-f pt.cfg';
        if (!isset($_REQUEST['text'])) {
            return serviceError('É necessário informar o texto');
        }else{
            $file = $this->dir . uniqid();
            file_put_contents($file, $_REQUEST['text']);
        }

        $exec = 'analyze ' . $conf. ' < ' . $file;
        echo $exec . PHP_EOL;
        exec($exec, $output, $returnvar);
        if ($returnvar == 0) {
            foreach ($output as $linha) {
                echo $linha;
            }
        } else {
            echo 'no return';
        }
    }
}

$ct = new Control();
$ct->processText();

//processaRequisicao();